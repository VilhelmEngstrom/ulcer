# Ulcer - Generic serialization library for C.

Provides generic, variadic serialization and deserialization. Requires a C11-compatible compiler.

[[_TOC_]]

## Example Usage

Ulcer is capable of serializing any integral type, or array consisting thereof. The typical usage looks something
akin to

```c
#include <ulcer/ulcer.h>
#include <stdint.h>

void serialize_example(void) {
    uint8_t u8 = 32u;
    uint32_t u32 = 42u;
    uint64_t u64 = 0xffffffffabull;

    ulcer_buf(buf, 1024);

    if(!ulcer_serialize(buf, u8, u32, u64)) {
        /* Insufficient buffer size */
    }
}

void deserialize_example(ulcer_buftype buf) {
    uint8_t u8;
    uint32_t u32;
    uint64_t u64;

    if(!ulcer_deserialize(buf, &u8, &u32, &u64)) {
        /* Insufficient number of serialized bytes in buffer */
    }
}.
```

Neither `ulcer_serialize` nor `ulcer_deserialize` modify the byte order of the serialized values. This is instead provided
via `ulcer_nwserialize` and `ulcer_nwdeserialize`, the first of which converts each element from host to network order whereas
the second performs the inverse operation.

### Ulcer Buffer

Ulcer works with an internal buffer type, instances of which are created using the `ulcer_buf` macro. This macro has two forms

```c
/* Two-parameter version. Storage is allocated on the stack.
 *
 * name     - buffer identifier (i.e. the name of the variable created by the macro)
 * capacity - desired capacity of the created buffer. Must be known at compile time
 */
ulcer_buf(name, capacity)

/* Three-parameter version. Storage allocated by user.
 *
 * name    - buffer identifier
 * addr    - pointer to array of unsigned char to be used for storage
 * size    - size of storage array */
ulcer_buf(name, addr, size)
```

The ulcer buffer is a fat pointer referring to a flexible array member in the buffer struct. The latter is defined as

```c
struct ulcer_buftype {
    size_t size;
    size_t capacity;
    size_t rdpos;
    unsigned char buf[]; /* <- address of the pointer  */
};
```

The above means that individual bytes in the ulcer buffer can be addressed by treating the buffer as a regular array of bytes.
After all, that is all it is.

```c
ulcer_buf(buf, 1024);
ulcer_serialize(buf, 0xdeadbeefu);

assert(buf[0] == 0xef);
assert(buf[1] == 0xbe);
assert(buf[2] == 0xad);
assert(buf[3] == 0xde);
```

The size (serialization index), read position (deserialization index) and capacity may be read using the `ulcer_size`, 
`ulcer_rdpos` and `ulcer_capacity` macros, respectively. Both size and read position may be altered by writing to the lvalue the macros expand to. The value yielded by `ulcer_capacity` is const qualified.

```c
ulcer_buf(buf, 1024);

/* Size */
assert(ulcer_size(buf) == 0);
ulcer_size(buf) = 32;
assert(ulcer_size(buf) == 32);

/* Read position */
assert(ulcer_rdpos(buf) == 0);
ulcer_rdpos(buf) = 28;
assert(ulcer_rdpos(buf) == 28);

/* Capacity */
assert(ulcer_capacity(size) == 1024);
```

### Working With Arrays

Ulcer requires some extra information for processing arrays. This is solved using the `ulcer_array` macro. If the array to be
serialized is a locally defined array, s.t. `sizeof(a)` for array `a` yields the size of the entire array in bytes rather than the size 
of a pointer to it, it is enough to pass only the array to `ulcer_array`

```c
ulcer_buf(buf, 1024);
uint32_t a[] = { 1, 2, 3 };
if(!ulcer_serialize(buf, ulcer_array(a))) {
    /* Insufficient buffer size */
}

/* buf now contains each element in a */
```

If the array has decayed to a pointer, it is the responsibility of the user to ensure the array size is passed as a second parameter, i.e.

```c
void serialize_decayed_array(uint32_t const *a, size_t size) {
    ulcer_buf(buf, 1024);
    if(!ulcer_serialize(buf, ulcer_array(a, size))) {
        /* Insufficient buffer size */
    }
}.
``` 

The two-parameter version of `ulcer_array` works with non-decayed arrays too.

Using arrays as output for deserialization is analogous to using them for serialization:

```c
ulcer_buf(buf, 1024);
unsigned a[3];
if(!ulcer_serialize(buf, 10u, 12u, 13u)) {
    /* Error */
}

if(!ulcer_deserialize(buf, ulcer_array(a, 3))) { /* size optional with array, required with pointer */
    /* Error */
}

assert(a[0] == 10u);
assert(a[1] == 12u);
assert(a[2] == 13u);
```

## Working with Sockets and Other Types of IO

Ulcer was designed for convenient use with Unix-style IO. This is achieved mainly by being able to treat the buffer as a simple array of unsigned chars. The following example shows how a buffer may be used to `recv` data from a network socket identified by the file descriptor `sockfd`. It goes without saying that the technique works just as well with functions such as `send`, `read`, `write`, etc.

```c
ulcer_buf(buf, 1024);

ulcer_size(buf) = recv(sockfd, buf, ulcer_capacity(buf), 0);

if(ulcer_size(buf) == -1) {
    perror("recv");
    exit(1);
}

if(!ulcer_nwdeserialize(buf, /* vars */)) {
    fputs("Deserialization error\n", stderr);
    exit(1);
}
/* Process data */

```

## Library Reference

#### `ulcer_buf(name, capacity)`

Create an ulcer buffer instance named `name` with capacity `capacity`. The underlying buffer is allocated on the stack by default. 
Prefixing the macro with `static`, e.g. `static ulcer_buf(buf, 128)`
yields a buffer with static storage duration instead.

#### `ulcer_buf(name, addr, size)`

Create an ulcer buffer instance named `name` with user-allocated storage at address `addr`. Note that the buffer is used for
storing the buffer header, meaning the capacity of the actual buffer is not `size`, but rather `size - sizeof(struct ulcer_buftype)`.

#### `bool ulcer_serialize(buf, ...)`

Serialize each variadic parameter and store result in the ulcer buffer `buf`. At most 63 variadic parameters, either integral types or 
arrays thereof, are supported. If `buf` already contains serialized data, `ulcer_serialize` appends rather than overwrites.

All parameters are treated as unsigned and are expected to be instances of integral types, or arrays of the same. Array parameters may 
be passed only using the `ulcer_array` macro.

Returns: `true` if all variadic parameters could be serialized.

#### `bool ulcer_nwserialize(buf, ...)`

Like `ulcer_serialize` but converts each serialized element to network byte order before writing them to the buffer. This applies recursively
to array, meaning each element in the array is converted to network order.

Returns: `true` if all variadic parameters could be serialized.

#### `bool ulcer_deserialize(buf, ...)`

Deserialize elements and store them in the addresses passed as variadic parameters. At most 63 variadic parameters are supported.
If data has already been read from `buf`, `ulcer_deserialize` resumes reading where the previous call left off.

All parameters are expected to be pointers to integral types, or arrays of integral types. Arrays are passed using the `ulcer_array` macro.
Deserialized values are treated as unsigned.

Returns: `true` if the buffer contained enough bytes to sate all variadic parameters.

#### `bool ulcer_nwdeserialize(buf, ...)`

Like `ulcer_deserialize` but converts each deserialized element to host byte order before writing them to the output parameters. Applies
recursively to each element in arrays.

Returns: `true` if all variadic parameters could be written

#### `ulcer_array(addr)`

Macro used for serializing an array at address `addr`. The single-parameter version works for non-decayed arrays, i.e. when, for an array 
`a`, `sizeof(a)` returns the size, in bytes, of the entire array. 

The single-parameter version always results in the entire array being serialized. More granularity can be gotten using the binary version.

#### `ulcer_array(addr, size)`

Like its single-parameter counterpart but works for decayed arrays. `size` is the number of elements in the array, i.e. **not** necessarily 
its size in bytes. This version of the macro may be used for serializing only parts of an array.

#### `ulcer_size(buf)`

Returns (expands to be precise) to the size of the buffer, i.e. the offset to the last byte written. The result is a signed lvalue, meaning the size can be altered by simply assigning the value. Note that setting a size larger than the capacity of the buffer will break ulcer.

#### `ulcer_rdpos(buf)`

Expands to a signed lvalue representing the offset of the next byte to be read from the buffer during deserialization. Much like with `ulcer_size`, the value may be altered by simply assigning it. Assigning a value greater than the current size is a poor idea.

#### `ulcer_capacity(buf)`

Expands to a signed, constant lvalue representing the capacity of the buffer.
