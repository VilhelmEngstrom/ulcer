#ifndef ULCER_H
#define ULCER_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <arpa/inet.h>
#include <sys/types.h>

#define ulcer_array_size(x) (sizeof(x) / (sizeof((x)[0])))

#define ulcer_is_integral(x)                                    \
    _Generic((x),                                               \
        uint8_t: 1,                                             \
        int8_t: 1,                                              \
        uint16_t: 1,                                            \
        int16_t: 1,                                             \
        uint32_t: 1,                                            \
        int32_t: 1,                                             \
        uint64_t: 1,                                            \
        int64_t: 1,                                             \
        default: 0)

#define ulcer_integral(val, i8, i16, i32, i64)                  \
    _Generic((val),                                             \
        uint8_t: i8,                                            \
        int8_t: i8,                                             \
        uint16_t: i16,                                          \
        int16_t: i16,                                           \
        uint32_t: i32,                                          \
        int32_t: i32,                                           \
        uint64_t: i64,                                          \
        int64_t: i64)

#define ulcer_packtype(val, i8, i16, i32, i64, arr)             \
    _Generic((val),                                             \
        uint8_t: i8,                                            \
        int8_t: i8,                                             \
        uint16_t: i16,                                          \
        int16_t: i16,                                           \
        uint32_t: i32,                                          \
        int32_t: i32,                                           \
        uint64_t: i64,                                          \
        int64_t: i64,                                           \
        struct ulcer_arraytype*: arr)

#define ulcer_unpacktype(val, i8, i16, i32, i64, arr)           \
    _Generic((*(val)),                                          \
        uint8_t: i8,                                            \
        int8_t: i8,                                             \
        uint16_t: i16,                                          \
        int16_t: i16,                                           \
        uint32_t: i32,                                          \
        int32_t: i32,                                           \
        uint64_t: i64,                                          \
        int64_t: i64,                                           \
        struct ulcer_arraytype: arr)

#define ulcer_hton(hostval) ulcer_integral(hostval, ulcer_identity8, htons, htonl, ulcer_htonll)(hostval)
#define ulcer_ntoh(netval) ulcer_integral(netval, ulcer_identity8, ntohs, ntohl, ulcer_ntohll)(netval)

#define ulcer_pack(buf, val) ulcer_packtype(val, ulcer_pack8, ulcer_pack16, ulcer_pack32, ulcer_pack64, ulcer_packarr)(buf, val)
#define ulcer_unpack(buf, val) ulcer_unpacktype(val, ulcer_unpack8, ulcer_unpack16, ulcer_unpack32, ulcer_unpack64, ulcer_unpackarr)(buf, val)

#define ulcer_packnw(buf, val) ulcer_packtype(val, ulcer_nwpack8, ulcer_nwpack16, ulcer_nwpack32, ulcer_nwpack64, ulcer_nwpackarr)(buf, val)
#define ulcer_unpacknw(buf, val) ulcer_unpacktype(val, ulcer_nwunpack8, ulcer_nwunpack16, ulcer_nwunpack32, ulcer_nwunpack64, ulcer_nwunpackarr)(buf, val)

#define ulcer_cat(a, b) a ## b
#define ulcer_cat_expand(a, b) ulcer_cat(a,b)

#define ulcer_str(x) #x
#define ulcer_str_expand(x) ulcer_str(x)

#define ulcer_count_seq 64, 63, 62, 61, 60, 59, 58, 57, \
                        56, 55, 54, 53, 52, 51, 50, 49, \
                        48, 47, 46, 45, 44, 43, 42, 41, \
                        40, 39, 38, 37, 36, 35, 34, 33, \
                        32, 31, 30, 29, 28, 27, 26, 25, \
                        24, 23, 22, 21, 20, 19, 18, 17, \
                        16, 15, 14, 13, 12, 11, 10, 9,  \
                        8,  7,  6,  5,  4,  3,  2,  1,  \
                        0

#define ulcer_count_pick(a0,  a1,  a2,  a3,  a4,  a5,  a6,  a7,     \
                         a8,  a9,  a10, a11, a12, a13, a14, a15,    \
                         a16, a17, a18, a19, a20, a21, a22, a23,    \
                         a24, a25, a26, a27, a28, a29, a30, a31,    \
                         a32, a33, a34, a35, a36, a37, a38, a39,    \
                         a40, a41, a42, a43, a44, a45, a46, a47,    \
                         a48, a49, a50, a51, a52, a53, a54, a55,    \
                         a56, a57, a58, a59, a60, a61, a62, a63,    \
                         a64, ...) a64

#define ulcer_expand(...) __VA_ARGS__
#define ulcer_count_expand(...) ulcer_count_pick(__VA_ARGS__)
#define ulcer_count(...) ulcer_count_expand(__VA_ARGS__, ulcer_count_seq)

#define ulcer_serialize(buf, ...) (ulcer_cat_expand(ulcer_recurse,ulcer_count(__VA_ARGS__))(ulcer_pack, buf, __VA_ARGS__))
#define ulcer_deserialize(buf, ...) (ulcer_cat_expand(ulcer_recurse,ulcer_count(__VA_ARGS__))(ulcer_unpack ,buf, __VA_ARGS__))

#define ulcer_nwserialize(buf, ...) (ulcer_cat_expand(ulcer_recurse,ulcer_count(__VA_ARGS__))(ulcer_packnw, buf, __VA_ARGS__))
#define ulcer_nwdeserialize(buf, ...) (ulcer_cat_expand(ulcer_recurse,ulcer_count(__VA_ARGS__))(ulcer_unpacknw, buf, __VA_ARGS__))

#define ulcer_recurse1(op, buf, val) op(buf, val)
#define ulcer_recurse2(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse1(op, buf, __VA_ARGS__)
#define ulcer_recurse3(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse2(op, buf, __VA_ARGS__)
#define ulcer_recurse4(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse3(op, buf, __VA_ARGS__)
#define ulcer_recurse5(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse4(op, buf, __VA_ARGS__)
#define ulcer_recurse6(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse5(op, buf, __VA_ARGS__)
#define ulcer_recurse7(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse6(op, buf, __VA_ARGS__)
#define ulcer_recurse8(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse7(op, buf, __VA_ARGS__)
#define ulcer_recurse9(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse8(op, buf, __VA_ARGS__)
#define ulcer_recurse10(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse9(op, buf, __VA_ARGS__)
#define ulcer_recurse11(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse10(op, buf, __VA_ARGS__)
#define ulcer_recurse12(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse11(op, buf, __VA_ARGS__)
#define ulcer_recurse13(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse12(op, buf, __VA_ARGS__)
#define ulcer_recurse14(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse13(op, buf, __VA_ARGS__)
#define ulcer_recurse15(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse14(op, buf, __VA_ARGS__)
#define ulcer_recurse16(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse15(op, buf, __VA_ARGS__)
#define ulcer_recurse17(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse16(op, buf, __VA_ARGS__)
#define ulcer_recurse18(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse17(op, buf, __VA_ARGS__)
#define ulcer_recurse19(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse18(op, buf, __VA_ARGS__)
#define ulcer_recurse20(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse19(op, buf, __VA_ARGS__)
#define ulcer_recurse21(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse20(op, buf, __VA_ARGS__)
#define ulcer_recurse22(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse21(op, buf, __VA_ARGS__)
#define ulcer_recurse23(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse22(op, buf, __VA_ARGS__)
#define ulcer_recurse24(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse23(op, buf, __VA_ARGS__)
#define ulcer_recurse25(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse24(op, buf, __VA_ARGS__)
#define ulcer_recurse26(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse25(op, buf, __VA_ARGS__)
#define ulcer_recurse27(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse26(op, buf, __VA_ARGS__)
#define ulcer_recurse28(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse27(op, buf, __VA_ARGS__)
#define ulcer_recurse29(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse28(op, buf, __VA_ARGS__)
#define ulcer_recurse30(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse29(op, buf, __VA_ARGS__)
#define ulcer_recurse31(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse30(op, buf, __VA_ARGS__)
#define ulcer_recurse32(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse31(op, buf, __VA_ARGS__)
#define ulcer_recurse33(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse32(op, buf, __VA_ARGS__)
#define ulcer_recurse34(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse33(op, buf, __VA_ARGS__)
#define ulcer_recurse35(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse34(op, buf, __VA_ARGS__)
#define ulcer_recurse36(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse35(op, buf, __VA_ARGS__)
#define ulcer_recurse37(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse36(op, buf, __VA_ARGS__)
#define ulcer_recurse38(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse37(op, buf, __VA_ARGS__)
#define ulcer_recurse39(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse38(op, buf, __VA_ARGS__)
#define ulcer_recurse40(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse39(op, buf, __VA_ARGS__)
#define ulcer_recurse41(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse40(op, buf, __VA_ARGS__)
#define ulcer_recurse42(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse41(op, buf, __VA_ARGS__)
#define ulcer_recurse43(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse42(op, buf, __VA_ARGS__)
#define ulcer_recurse44(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse43(op, buf, __VA_ARGS__)
#define ulcer_recurse45(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse44(op, buf, __VA_ARGS__)
#define ulcer_recurse46(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse45(op, buf, __VA_ARGS__)
#define ulcer_recurse47(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse46(op, buf, __VA_ARGS__)
#define ulcer_recurse48(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse47(op, buf, __VA_ARGS__)
#define ulcer_recurse49(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse48(op, buf, __VA_ARGS__)
#define ulcer_recurse50(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse49(op, buf, __VA_ARGS__)
#define ulcer_recurse51(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse50(op, buf, __VA_ARGS__)
#define ulcer_recurse52(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse51(op, buf, __VA_ARGS__)
#define ulcer_recurse53(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse52(op, buf, __VA_ARGS__)
#define ulcer_recurse54(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse53(op, buf, __VA_ARGS__)
#define ulcer_recurse55(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse54(op, buf, __VA_ARGS__)
#define ulcer_recurse56(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse55(op, buf, __VA_ARGS__)
#define ulcer_recurse57(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse56(op, buf, __VA_ARGS__)
#define ulcer_recurse58(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse57(op, buf, __VA_ARGS__)
#define ulcer_recurse59(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse58(op, buf, __VA_ARGS__)
#define ulcer_recurse60(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse59(op, buf, __VA_ARGS__)
#define ulcer_recurse61(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse60(op, buf, __VA_ARGS__)
#define ulcer_recurse62(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse61(op, buf, __VA_ARGS__)
#define ulcer_recurse63(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse62(op, buf, __VA_ARGS__)
#define ulcer_recurse64(op, buf, val, ...) ulcer_recurse1(op, buf, val) && ulcer_recurse63(op, buf, __VA_ARGS__)

#define ulcer_panic(msg)                                                                                \
    do {                                                                                                \
        fputs(msg "\n", stderr);                                                                        \
        exit(1);                                                                                        \
    } while (0)

#define ulcer_assert(cond, msg)                                                                         \
    if(!(cond)) {                                                                                       \
        ulcer_panic("Assertion " #cond " on line " ulcer_str_expand(__LINE__) " failed: " msg);         \
    }

#define ulcer_embed_static_assert(cond, msg, expr) \
    (ulcer_identity8(!!(unsigned char[(cond << 1) >> 1]) { 0 }), expr)

#define ulcer_buf(name, ...) \
    ulcer_cat_expand(ulcer_buf,ulcer_count(__VA_ARGS__))(name, __VA_ARGS__)

#define ulcer_buf1(name, cap)                                           \
    ulcer_buftype *name = ulcer_init(&(union {                          \
        unsigned char u_bytes[sizeof(struct ulcer_buflayout) + cap];    \
        struct ulcer_buflayout u_layout;                                \
    }){ 0 }.u_layout, cap);

#define ulcer_buf2(name, ptr, cap)                                      \
    ulcer_buftype *name = ulcer_init_raw(ptr, cap)

enum ulcer_endian {
    ulcer_endian_little,
    ulcer_endian_big
};

struct ulcer_buflayout {
    ssize_t size;
    ssize_t cap;
    ssize_t rdpos;
    unsigned char buf[];
};

struct ulcer_arraytype {
    void *addr;
    size_t elemsize;
    size_t size;
};

typedef unsigned char ulcer_buftype;

inline ulcer_buftype *ulcer_init(struct ulcer_buflayout *layout, size_t cap) {
    layout->size = 0;
    layout->cap = cap;
    layout->rdpos = 0;
    return layout->buf;
}

inline ulcer_buftype *ulcer_init_raw(unsigned char *raw, size_t cap) {
    ulcer_assert(cap > sizeof(struct ulcer_buflayout), "Insufficient capacity to hold metadata");
    struct ulcer_buflayout *layout = &((union { unsigned char b[1]; struct ulcer_buflayout layout; } *)raw)->layout;
    return ulcer_init(layout, cap - sizeof(*layout));
}


#define ulcer_array(...) \
    ulcer_cat_expand(ulcer_array,ulcer_count(__VA_ARGS__))(__VA_ARGS__)

#define ulcer_array1(p)         \
    ulcer_embed_static_assert(ulcer_is_integral(*(p)), "Only integral types are supported", (&(struct ulcer_arraytype) { .addr = p, .elemsize = sizeof(*(p)), .size = ulcer_array_size(p) }))
#define ulcer_array2(p, len)    \
    ulcer_embed_static_assert(ulcer_is_integral(*(p)), "Only integral types are supported", (&(struct ulcer_arraytype) { .addr = p, .elemsize = sizeof(*(p)), .size = len }))

#define ulcer_container(...) ulcer_cat_expand(ulcer_container,ulcer_count(__VA_ARGS__))(__VA_ARGS__)
#define ulcer_container4(addr, type, member, qual)      \
    ((type qual *)((unsigned char qual *)addr - offsetof(type, member)))
#define ulcer_container3(addr, type, member)            \
    ulcer_container4(addr, type, member,)

#define ulcer_size(addr)    \
    (ulcer_container(addr, struct ulcer_buflayout, buf)->size)
#define ulcer_bufcap(addr)  \
    (ulcer_container(addr, struct ulcer_buflayout, buf)->cap)
#define ulcer_rdpos(addr)   \
    (ulcer_container(addr, struct ulcer_buflayout, buf)->rdpos)
#define ulcer_capacity(add) \
    ((ssize_t const)ulcer_bufcap(addr))

inline uint8_t ulcer_identity8(uint8_t v) {
    return v;
}

uint64_t ulcer_htonll(uint64_t hostll);
uint64_t ulcer_ntohll(uint64_t netll);

bool ulcer_pack8(ulcer_buftype *restrict buf, uint8_t v);
bool ulcer_pack16(ulcer_buftype *restrict buf, uint16_t v);
bool ulcer_pack32(ulcer_buftype *restrict buf, uint32_t v);
bool ulcer_pack64(ulcer_buftype *restrict buf, uint64_t v);
bool ulcer_packarr(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr);
bool ulcer_nwpackarr(ulcer_buftype *restrict buf, struct ulcer_arraytype const *restrict arr);


bool ulcer_unpack8(ulcer_buftype *restrict buf, uint8_t *restrict v);
bool ulcer_unpack16(ulcer_buftype *restrict buf, uint16_t *restrict v);
bool ulcer_unpack32(ulcer_buftype *restrict buf, uint32_t *restrict v);
bool ulcer_unpack64(ulcer_buftype *restrict buf, uint64_t *restrict v);
bool ulcer_unpackarr(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr);
bool ulcer_nwunpackarr(ulcer_buftype *restrict buf, struct ulcer_arraytype *restrict arr);

inline bool ulcer_nwpack8(ulcer_buftype *restrict buf, uint8_t v) {
    return ulcer_pack(buf, ulcer_hton(v));
}

inline bool ulcer_nwpack16(ulcer_buftype *restrict buf, uint16_t v) {
    return ulcer_pack(buf, ulcer_hton(v));
}

inline bool ulcer_nwpack32(ulcer_buftype *restrict buf, uint32_t v) {
    return ulcer_pack(buf, ulcer_hton(v));
}

inline bool ulcer_nwpack64(ulcer_buftype *restrict buf, uint64_t v) {
    return ulcer_pack(buf, ulcer_hton(v));
}

inline bool ulcer_nwunpack8(ulcer_buftype *restrict buf, uint8_t *restrict v) {
    if(!ulcer_unpack(buf, v)) {
        return false;
    }
    *v = ulcer_ntoh(*v);
    return true;
}
inline bool ulcer_nwunpack16(ulcer_buftype *restrict buf, uint16_t *restrict v) {
    if(!ulcer_unpack(buf, v)) {
        return false;
    }
    *v = ulcer_ntoh(*v);
    return true;
}

inline bool ulcer_nwunpack32(ulcer_buftype *restrict buf, uint32_t *restrict v) {
    if(!ulcer_unpack(buf, v)) {
        return false;
    }
    *v = ulcer_ntoh(*v);
    return true;
}

inline bool ulcer_nwunpack64(ulcer_buftype *restrict buf, uint64_t *restrict v) {
    if(!ulcer_unpack(buf, v)) {
        return false;
    }
    *v = ulcer_ntoh(*v);
    return true;
}

inline enum ulcer_endian ulcer_endianness(void) {
    return htons(0xdeadu) == (uint16_t)0xdeadu;
}

#endif /* ULCER_H */
