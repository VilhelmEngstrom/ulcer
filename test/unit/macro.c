#include <cldm/cldm.h>
#include <ulcer/ulcer.h>

TEST(count) {
    ASSERT_EQ(ulcer_count(1, 2, 3), 3);
    ASSERT_EQ(ulcer_count(,,,,,,), 7);
}
