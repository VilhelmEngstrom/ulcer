#include <cldm/cldm.h>
#include <ulcer/ulcer.h>

#include <stdio.h>
#include <string.h>

TEST(serialize) {
    union {
        uint16_t u16;
        uint32_t u32;
        uint64_t u64;
    } un_aligned;
    memset(&un_aligned, 0, sizeof(un_aligned));

    ulcer_buf(buf, 1024);
    ASSERT_TRUE(ulcer_serialize(buf, (uint8_t)1, (uint16_t)2, (uint32_t)3, (uint64_t)4));
    ASSERT_EQ(buf[0], 1);
    memcpy(&un_aligned.u16, buf + sizeof(uint8_t), sizeof(un_aligned.u16));
    ASSERT_EQ(un_aligned.u16, 2);
    memcpy(&un_aligned.u32, buf + sizeof(uint8_t) + sizeof(uint16_t), sizeof(un_aligned.u32));
    ASSERT_EQ(un_aligned.u32, 3);
    memcpy(&un_aligned.u64, buf + sizeof(uint8_t) + sizeof(uint16_t) + sizeof(uint32_t), sizeof(un_aligned.u64));
    ASSERT_EQ(un_aligned.u64, 4);

    ASSERT_TRUE(ulcer_serialize(buf, (uint64_t)0xdeadbeefabeull));
    ASSERT_EQ(buf[0], 1);
    memcpy(&un_aligned.u16, buf + sizeof(uint8_t), sizeof(un_aligned.u16));
    ASSERT_EQ(un_aligned.u16, 2);
    memcpy(&un_aligned.u32, buf + sizeof(uint8_t) + sizeof(uint16_t), sizeof(un_aligned.u32));
    ASSERT_EQ(un_aligned.u32, 3);
    memcpy(&un_aligned.u64, buf + sizeof(uint8_t) + sizeof(uint16_t) + sizeof(uint32_t), sizeof(un_aligned.u64));
    ASSERT_EQ(un_aligned.u64, 4);
    memcpy(&un_aligned.u64, buf + sizeof(uint8_t) + sizeof(uint16_t) + sizeof(uint32_t) + sizeof(uint64_t), sizeof(un_aligned.u64));
    ASSERT_EQ(un_aligned.u64, 0xdeadbeefabeull);
}

TEST(serialize_array) {
    ulcer_buf(buf, 1024);
    uint32_t aligned = 0;
    uint32_t arr[32] = { 1,2,3,4,5 };
    ASSERT_TRUE(ulcer_serialize(buf, ulcer_array(arr), (uint32_t)10));
    ASSERT_EQ(ulcer_size(buf), sizeof(arr) + sizeof(uint32_t));
    memcpy(&aligned, &buf[3 * sizeof(arr[0])], sizeof(aligned));
    ASSERT_EQ(aligned, arr[3]);
    memcpy(&aligned, &buf[sizeof(arr)], sizeof(aligned));
    ASSERT_EQ(aligned, 10);
}

TEST(deserialize) {
    ulcer_buf(buf, 1024);
    ASSERT_TRUE(ulcer_serialize(buf, (uint8_t)8, (uint16_t)16, (uint32_t)32, (uint64_t)64));
    uint8_t u8 = 0;
    uint16_t u16 = 0;
    uint32_t u32 = 0;
    uint64_t u64 = 0;
    ASSERT_TRUE(ulcer_deserialize(buf, &u8, &u16, &u32, &u64));
    ASSERT_EQ(u8, 8);
    ASSERT_EQ(u16, 16);
    ASSERT_EQ(u32, 32);
    ASSERT_EQ(u64, 64);
}

TEST(deserialize_array) {
    ulcer_buf(buf, 1024);
    ASSERT_TRUE(ulcer_serialize(buf, (uint32_t)8, (uint32_t)12, (uint32_t)16, (uint32_t)20));
    uint32_t arr[4];
    ASSERT_TRUE(ulcer_deserialize(buf, ulcer_array(arr)));
    ASSERT_EQ(arr[0], 8);
    ASSERT_EQ(arr[1], 12);
    ASSERT_EQ(arr[2], 16);
    ASSERT_EQ(arr[3], 20);
}

TEST(nwserialize) {
    union {
        uint8_t u8;
        uint16_t u16;
        uint32_t u32;
        uint64_t u64;
    } un_aligned;
    memset(&un_aligned, 0, sizeof(un_aligned));
    ulcer_buf(buf, 1024);

    ASSERT_TRUE(ulcer_nwserialize(buf, (uint8_t)1u, (uint16_t)(1u << 8u), (uint32_t)(1u << 24u), (uint64_t)(1ull << 48u)));
    memcpy(&un_aligned.u8, buf, sizeof(un_aligned.u8));
    ASSERT_EQ(un_aligned.u8, 1u);
    memcpy(&un_aligned.u16, buf + sizeof(un_aligned.u8), sizeof(un_aligned.u16));
    ASSERT_EQ(un_aligned.u16, htons(1u << 8u));
    memcpy(&un_aligned.u32, buf + sizeof(un_aligned.u8) + sizeof(un_aligned.u16), sizeof(un_aligned.u32));
    ASSERT_EQ(un_aligned.u32, htonl(1u << 24u));
    memcpy(&un_aligned.u64, buf + sizeof(un_aligned.u8) + sizeof(un_aligned.u16) + sizeof(un_aligned.u32), sizeof(un_aligned.u64));
    ASSERT_EQ(un_aligned.u64, ulcer_htonll(1ull << 48u));
}

TEST(nwserialize_array) {
    ulcer_buf(buf, 1024);
    uint32_t aligned = 0u;
    uint32_t arr[32] = { 1u, 1u << 8u, 1u << 16u, 1u << 24u };
    ASSERT_TRUE(ulcer_nwserialize(buf, ulcer_array(arr)));
    memcpy(&aligned, buf, sizeof(aligned));
    ASSERT_EQ(aligned, ntohl(1u));
    memcpy(&aligned, buf + sizeof(aligned), sizeof(aligned));
    ASSERT_EQ(aligned, ntohl(1u << 8u));
    memcpy(&aligned, buf + 2 * sizeof(aligned), sizeof(aligned));
    ASSERT_EQ(aligned, ntohl(1u << 16u));
    memcpy(&aligned, buf + 3 * sizeof(aligned), sizeof(aligned));
    ASSERT_EQ(aligned, ntohl(1u << 24u));
}

TEST(nwdeserialize) {
    ulcer_buf(buf, 1024);
    uint8_t u8 = 0u;
    uint16_t u16 = 0u;
    uint32_t u32 = 0u;
    uint64_t u64 = 0u;
    ASSERT_TRUE(ulcer_nwserialize(buf, (uint8_t)10u, (uint16_t)(10u << 8u), (uint32_t)(10u << 16u), (uint64_t)(10ull << 48u)));
    ASSERT_TRUE(ulcer_nwdeserialize(buf, &u8, &u16, &u32, &u64));
    ASSERT_EQ(u8, 10u);
    ASSERT_EQ(u16, 10u << 8u);
    ASSERT_EQ(u32, 10u << 16u);
    ASSERT_EQ(u64, 10ull << 48u);
}

TEST(nwdeserialize_array) {
    ulcer_buf(buf, 1024);
    uint32_t arr[4] = { 0 };

    ASSERT_TRUE(ulcer_nwserialize(buf, (uint32_t)10u, (uint32_t)11u, (uint32_t)12u, (uint32_t)13u));
    ASSERT_TRUE(ulcer_nwdeserialize(buf, ulcer_array(arr)));
    ASSERT_EQ(arr[0], 10u);
    ASSERT_EQ(arr[1], 11u);
    ASSERT_EQ(arr[2], 12u);
    ASSERT_EQ(arr[3], 13u);
}

TEST(serialize_partial_array) {
    ulcer_buf(buf, 1024);
    uint32_t u0 = 0u, u1 = 0u, u2 = 0u;
    uint32_t arr[] = { 1,2,3,4,5 };
    ASSERT_TRUE(ulcer_serialize(buf, ulcer_array(&arr[1], 3u)));
    ASSERT_EQ(ulcer_size(buf), 3 * sizeof(uint32_t));
    ASSERT_TRUE(ulcer_deserialize(buf, &u0, &u1, &u2));
    ASSERT_EQ(u0, 2u);
    ASSERT_EQ(u1, 3u);
    ASSERT_EQ(u2, 4u);
}

