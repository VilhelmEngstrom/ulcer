#include <cldm/cldm.h>
#include <ulcer/ulcer.h>

TEST(overflow_serialize) {
    ulcer_buf(buf, 2 * sizeof(uint32_t) + sizeof(uint16_t));
    ASSERT_TRUE(ulcer_serialize(buf, (uint32_t)0u, (uint32_t)0u, (uint16_t)0u));
    ulcer_size(buf) = ulcer_size(buf) - 1u;
    ASSERT_TRUE(ulcer_serialize(buf, (uint8_t)0u));
    ulcer_size(buf) = ulcer_size(buf) - 1u;
    ASSERT_FALSE(ulcer_serialize(buf, (uint16_t)0u));
    ulcer_size(buf) = 0u;
    ASSERT_FALSE(ulcer_serialize(buf, (uint32_t)0u, (uint32_t)0u, (uint16_t)0u, (uint8_t)0u));
}

TEST(overflow_deserialize) {
    ulcer_buf(buf, 3 * sizeof(uint32_t));
    uint8_t u8;
    uint32_t u32;;
    ASSERT_TRUE(ulcer_serialize(buf, (uint32_t)0u, (uint32_t)1u, (uint32_t)2u));
    ASSERT_TRUE(ulcer_deserialize(buf, &u32, &u32, &u32));
    ASSERT_FALSE(ulcer_deserialize(buf, &u8));
    ulcer_rdpos(buf) = ulcer_rdpos(buf) - 1u;
    ASSERT_TRUE(ulcer_deserialize(buf, &u8));
    ulcer_rdpos(buf) = ulcer_rdpos(buf) - 1u;
    ASSERT_FALSE(ulcer_deserialize(buf, &u32));
}
