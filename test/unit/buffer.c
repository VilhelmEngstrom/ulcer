#include <cldm/cldm.h>
#include <ulcer/ulcer.h>

#include <stdlib.h>

TEST(buffer_size) {
    ulcer_buf(buf, 1024);
    uint8_t arr[31] = { 0 };
    ASSERT_EQ(ulcer_size(buf), 0);
    ASSERT_TRUE(ulcer_serialize(buf, 10u));
    ASSERT_EQ(ulcer_size(buf), sizeof(10u));
    ASSERT_TRUE(ulcer_serialize(buf, 10u, 11u, 12u, 13u, ulcer_array(arr)));
    ASSERT_EQ(ulcer_size(buf), 5 * sizeof(10u) + sizeof(arr));
}

TEST(buffer_capacity) {
    {
        ulcer_buf(buf, 1024);
        ASSERT_EQ(ulcer_bufcap(buf), 1024);
    }
    {
        ulcer_buf(buf, 3);
        ASSERT_EQ(ulcer_bufcap(buf), 3);
    }
    {
        ulcer_buf(buf, 28);
        ASSERT_EQ(ulcer_bufcap(buf), 28);
    }
}

TEST(buffer_readpos) {
    ulcer_buf(buf, 1024);
    uint32_t val = 0;
    ASSERT_EQ(ulcer_rdpos(buf), 0);
    ASSERT_TRUE(ulcer_serialize(buf, val, val, val, val));
    ASSERT_EQ(ulcer_rdpos(buf), 0);
    ASSERT_TRUE(ulcer_deserialize(buf, &val, &val));
    ASSERT_EQ(ulcer_rdpos(buf), 2 * sizeof(val));
    ASSERT_TRUE(ulcer_deserialize(buf, &val));
    ASSERT_EQ(ulcer_rdpos(buf), 3 * sizeof(val));
    ASSERT_TRUE(ulcer_deserialize(buf, &val));
    ASSERT_EQ(ulcer_rdpos(buf), 4 * sizeof(val));
}

TEST(mallocd_buffer) {
    unsigned char *raw = malloc(1024);
    ASSERT_TRUE(raw);
    if(!raw) {
        return;
    }

    ulcer_buf(buf, raw, 1024);

    ASSERT_EQ(buf, raw + sizeof(struct ulcer_buflayout));
    ASSERT_EQ(ulcer_bufcap(buf), 1024 - sizeof(struct ulcer_buflayout));
    ASSERT_EQ(ulcer_size(buf), 0);
    ASSERT_EQ(ulcer_rdpos(buf), 0);

    free(raw);
}

TEST(byte_access) {
    ulcer_buf(buf, 1024);
    ASSERT_TRUE(ulcer_serialize(buf, 0xdeadbeefu));
    ASSERT_EQ(buf[0], 0xef);
    ASSERT_EQ(buf[1], 0xbe);
    ASSERT_EQ(buf[2], 0xad);
    ASSERT_EQ(buf[3], 0xde);
}
