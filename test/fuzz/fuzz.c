#include <ulcer/ulcer.h>

#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define align(addr, boundary)   \
    (void *)(((uintptr_t)addr + (boundary - 1u)) & -boundary)

bool match(void const *restrict b0, void const *restrict b1, size_t nelems, size_t elemsize) {
    bool b = true;
    switch(elemsize) {
        case sizeof(uint8_t):
            for(unsigned i = 0; i < nelems; i++) {
                if(((uint8_t *)b0)[i] != ((uint8_t *)b1)[i]) {
                    fprintf(stderr, "Byte %u differs: %" PRIu8 " != %" PRIu8 "\n", i, ((uint8_t *)b0)[i], ((uint8_t *)b1)[i]);
                    b = false;
                }
            }
            break;
        case sizeof(uint16_t):
            for(unsigned i = 0; i < nelems; i++) {
                if(((uint16_t *)b0)[i] != ((uint16_t *)b1)[i]) {
                    fprintf(stderr, "Word %u differs: %" PRIu16 " != %" PRIu16 "\n", i, ((uint16_t *)b0)[i], ((uint16_t *)b1)[i]);
                    b = false;
                }
            }
            break;
        case sizeof(uint32_t):
            for(unsigned i = 0; i < nelems; i++) {
                if(((uint32_t *)b0)[i] != ((uint32_t *)b1)[i]) {
                    fprintf(stderr, "Dword %u differs: %" PRIu32 " != %" PRIu32 "\n", i, ((uint32_t *)b0)[i], ((uint32_t *)b1)[i]);
                    b = false;
                }
            }
            break;
        case sizeof(uint64_t):
            for(unsigned i = 0; i < nelems; i++) {
                if(((uint64_t *)b0)[i] != ((uint64_t *)b1)[i]) {
                    fprintf(stderr, "Qword %u differs: %" PRIu64 " != %" PRIu64 "\n", i, ((uint64_t *)b0)[i], ((uint64_t *)b1)[i]);
                    b = false;
                }
            }
            break;
        default:
            b = false;
            break;
    }
    return b;
}

void report_error(char const *restrict type, char const *restrict op, size_t nelems, size_t elemsize, size_t mallocsize, size_t size, ulcer_buftype *restrict buf) {
    fprintf(stderr, "Error while %s:%s\n", op, type);
    fprintf(stderr, "  Number of elements: %zu\n", nelems);
    fprintf(stderr, "  Element size: %zu\n", elemsize);
    fprintf(stderr, "  Mallocd size: %zu\n", mallocsize);
    fprintf(stderr, "  Aligned input size: %zu\n", size);
    fprintf(stderr, "  Buffer info:\n");
    fprintf(stderr, "    size: %zu\n", ulcer_size(buf));
    fprintf(stderr, "    rdpos: %zu\n", ulcer_rdpos(buf));
    fprintf(stderr, "    capacity: %zu\n", ulcer_bufcap(buf));
}

int LLVMFuzzerTestOneInput(uint8_t const *data, size_t size) {
    if(!size) {
        return 0;
    }
    uintptr_t boundary;
    size_t elemsize;
    size_t nelems;

    void *aligned_data;

    void *deserialized;
    void *nw_deserialized;

    bool success;
    bool nw_success;

    bool crash = true;

    size_t bufsize = size + sizeof(struct ulcer_buflayout);
    uint8_t *raw = malloc(bufsize);
    uint8_t *nwraw = malloc(bufsize);

    void *deserialized_raw = malloc(size + _Alignof(uint64_t) + sizeof(uint64_t));
    void *nw_deserialized_raw = malloc(size + _Alignof(uint64_t) + sizeof(uint64_t));

    if(!raw || !nwraw || !deserialized_raw || !nw_deserialized_raw) {
        fputs("malloc failure\n", stderr);
        goto epilogue;
    }

    ulcer_buf(buf, raw, bufsize);
    ulcer_buf(nwbuf, nwraw, bufsize);

    /* Interpret data as
     * Bytes if data[0] in range 0-63
     * Words if data[0] in range 64-127
     * Dwords if data[0] in range 128-191
     * Qwords if data[0] in range 192-255 */
    switch(data[0] >> 6) {
        case 0u:
            boundary = _Alignof(uint8_t);
            elemsize = sizeof(uint8_t);
            break;
        case 1u:
            boundary = _Alignof(uint16_t);
            elemsize = sizeof(uint16_t);
            break;
        case 2u:
            boundary = _Alignof(uint32_t);
            elemsize = sizeof(uint32_t);
            break;
        case 3u:
            boundary = _Alignof(uint64_t);
            elemsize = sizeof(uint64_t);
            break;
        default:
            goto epilogue;
    }
    aligned_data = align(data, boundary);
    deserialized = align(deserialized_raw, boundary);
    nw_deserialized = align(nw_deserialized_raw, boundary);

    /* Check that there is at least 1 element in aligned buffer */
    if((uint8_t *)aligned_data - data + elemsize < size) {
        crash = false;
        goto epilogue;
    }

    size -= ((uint8_t *)aligned_data - data);
    nelems = size / elemsize;

    switch(elemsize) {
        case sizeof(uint8_t):
            success = ulcer_serialize(buf, ulcer_array((uint8_t *)aligned_data, nelems));
            nw_success = ulcer_nwserialize(nwbuf, ulcer_array((uint8_t *)aligned_data, nelems));
            break;
        case sizeof(uint16_t):
            success = ulcer_serialize(buf, ulcer_array((uint16_t *)aligned_data, nelems));
            nw_success = ulcer_nwserialize(nwbuf, ulcer_array((uint16_t *)aligned_data, nelems));
            break;
        case sizeof(uint32_t):
            success = ulcer_serialize(buf, ulcer_array((uint32_t *)aligned_data, nelems));
            nw_success = ulcer_nwserialize(nwbuf, ulcer_array((uint32_t *)aligned_data, nelems));
            break;
        case sizeof(uint64_t):
            success = ulcer_serialize(buf, ulcer_array((uint64_t *)aligned_data, nelems));
            nw_success = ulcer_nwserialize(nwbuf, ulcer_array((uint64_t *)aligned_data, nelems));
            break;
        default:
            goto epilogue;
    }
    if(!success) {
        report_error("non-nw", "serializing", nelems, elemsize, bufsize, size, buf);
        goto epilogue;
    }
    if(!nw_success) {
        report_error("nw", "serializing", nelems, elemsize, bufsize, size, buf);
        goto epilogue;
    }

    switch(elemsize) {
        case sizeof(uint8_t):
            success = ulcer_deserialize(buf, ulcer_array((uint8_t *)deserialized, nelems));
            nw_success = ulcer_nwdeserialize(nwbuf, ulcer_array((uint8_t *)nw_deserialized, nelems));
            break;
        case sizeof(uint16_t):
            success = ulcer_deserialize(buf, ulcer_array((uint16_t *)deserialized, nelems));
            nw_success = ulcer_nwdeserialize(nwbuf, ulcer_array((uint16_t *)nw_deserialized, nelems));
            break;
        case sizeof(uint32_t):
            success = ulcer_deserialize(buf, ulcer_array((uint32_t *)deserialized, nelems));
            nw_success = ulcer_nwdeserialize(nwbuf, ulcer_array((uint32_t *)nw_deserialized, nelems));
            break;
        case sizeof(uint64_t):
            success = ulcer_deserialize(buf, ulcer_array((uint64_t *)deserialized, nelems));
            nw_success = ulcer_nwdeserialize(nwbuf, ulcer_array((uint64_t *)nw_deserialized, nelems));
            break;
        default:
            goto epilogue;
    }
    if(!success) {
        report_error("non-nw", "deserializing", nelems, elemsize, bufsize, size, buf);
        goto epilogue;
    }
    if(!nw_success) {
        report_error("nw", "deserializing", nelems, elemsize, bufsize, size, buf);
        goto epilogue;
    }

    if(!match(aligned_data, deserialized, nelems, elemsize)) {
        report_error("non-nw", "matching", nelems, elemsize, bufsize, size, buf);
        goto epilogue;
    }
    if(!match(aligned_data, nw_deserialized, nelems, elemsize)) {
        report_error("nw", "matching", nelems, elemsize, bufsize, size, buf);
        goto epilogue;
    }

    ulcer_rdpos(buf) = 0u;
    ulcer_rdpos(nwbuf) = 0u;

    switch(elemsize) {
        case sizeof(uint8_t):
            success = ulcer_nwdeserialize(buf, ulcer_array((uint8_t *)deserialized, nelems + 1u));
            nw_success = ulcer_nwdeserialize(nwbuf, ulcer_array((uint8_t *)nw_deserialized, nelems + 1u));
            break;
        case sizeof(uint16_t):
            success = ulcer_nwdeserialize(buf, ulcer_array((uint16_t *)deserialized, nelems + 1u));
            nw_success = ulcer_nwdeserialize(nwbuf, ulcer_array((uint16_t *)nw_deserialized, nelems + 1u));
            break;
        case sizeof(uint32_t):
            success = ulcer_nwdeserialize(buf, ulcer_array((uint32_t *)deserialized, nelems + 1u));
            nw_success = ulcer_nwdeserialize(nwbuf, ulcer_array((uint32_t *)nw_deserialized, nelems + 1u));
            break;
        case sizeof(uint64_t):
            success = ulcer_nwdeserialize(buf, ulcer_array((uint64_t *)deserialized, nelems + 1u));
            nw_success = ulcer_nwdeserialize(nwbuf, ulcer_array((uint64_t *)nw_deserialized, nelems + 1u));
            break;
        default:
            goto epilogue;
    }
    if(success) {
        report_error("non-nw", "deserializing with overflow", nelems, elemsize, bufsize, size, buf);
        goto epilogue;
    }
    if(nw_success) {
        report_error("nw", "deserializing with overflow", nelems, elemsize, bufsize, size, buf);
        goto epilogue;
    }

    crash = false;
epilogue:
    if(raw) {
        free(raw);
    }
    if(nwraw) {
        free(nwraw);
    }
    if(deserialized_raw) {
        free(deserialized_raw);
    }
    if(nw_deserialized_raw) {
        free(nw_deserialized_raw);
    }
    if(crash) {
        abort();
    }
    return 0;
}
